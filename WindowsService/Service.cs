﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using WindowsService.Engine.Scheduler;

namespace WindowsService
{
    public partial class Service : ServiceBase
    {
        public SystemScheduler systemScheduler;

        public Service()
        {
            InitializeComponent();
            EventLog.Log = "Select Windows Service";
        }

        protected override void OnStart(string[] args)
        {
            systemScheduler = new SystemScheduler();
            systemScheduler.Run();
        }

        protected override void OnStop()
        {
            systemScheduler.Stop();
        }
    }
}
