﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.IO;
using System.Reflection;

namespace WindowsService.Core.Utils
{
    public class EventLogger
    {
        private static string ProgramFiles;
        private static string Manufacturer;
        private static string Product;
        private static string LogPath;

        const string source = "SelectReportGeneratorService";
        const string LogName = @"Error.log";
        private static StreamWriter log;

        static EventLogger()
        {
            try
            {
                if (!EventLog.SourceExists(source))
                {
                    EventLog.CreateEventSource(source, "Select Report Generator");
                    Thread.Sleep(500);
                }

                ProgramFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
                Manufacturer = GetAssemblyAttribute<AssemblyCompanyAttribute>(a => a.Company);
                Product = GetAssemblyAttribute<AssemblyProductAttribute>(a => a.Product);
                LogPath = ProgramFiles + Manufacturer + @"\" + Product + @"\Log\";
            }
            catch (Exception ex)
            {
                try
                {
                    if (!File.Exists(LogPath + LogName))
                    {
                        log = new StreamWriter(LogPath + LogName);
                    }
                    else
                    {
                        log = File.AppendText(LogPath + LogName);
                    }

                    log.WriteLine(DateTime.Now);
                    log.WriteLine(ex.Message);
                    if (ex.InnerException != null)
                    {
                        log.WriteLine("Inner Exception");
                        log.WriteLine(ex.Message);
                    }
                    log.WriteLine();
                    log.Close();
                }
                catch { }
            }
        }

        public void SystemLog(string process, bool positive, int errorID, string comment)
        {
            try { Post(positive, errorID, string.Format("{0}\r\n\r\n{1}", process, comment)); }
            catch (Exception ex)
            {
                try
                {
                    if (!File.Exists(LogPath + LogName))
                    {
                        log = new StreamWriter(LogPath + LogName);
                    }
                    else
                    {
                        log = File.AppendText(LogPath + LogName);
                    }

                    log.WriteLine(DateTime.Now);
                    log.WriteLine(ex.Message);
                    if (ex.InnerException != null)
                    {
                        log.WriteLine("Inner Exception");
                        log.WriteLine(ex.Message);
                    }
                    log.WriteLine();
                    log.Close();
                }
                catch { }
            }
        }

        public static void Post(bool positive, int errorID, string message)
        {
            try { EventLog.WriteEntry(source, message, (positive ? EventLogEntryType.Information : EventLogEntryType.Error), errorID); }
            catch (Exception ex)
            {
                try
                {
                    if (!File.Exists(LogPath + LogName))
                    {
                        log = new StreamWriter(LogPath + LogName);
                    }
                    else
                    {
                        log = File.AppendText(LogPath + LogName);
                    }

                    log.WriteLine(DateTime.Now);
                    log.WriteLine(ex.Message);
                    if (ex.InnerException != null)
                    {
                        log.WriteLine("Inner Exception");
                        log.WriteLine(ex.Message);
                    }
                    log.WriteLine();
                    log.Close();
                }
                catch { }
            }
        }

        public static void Post(Exception ex, int errorID, string comment, string inner = "")
        {
            try { Post(false, errorID, string.Format("{0}\r\n\r\n{1}\r\n\r\n{2}", comment, ex.ToString(), inner)); }
            catch (Exception exc)
            {
                try
                {
                    if (!File.Exists(LogPath + LogName))
                    {
                        log = new StreamWriter(LogPath + LogName);
                    }
                    else
                    {
                        log = File.AppendText(LogPath + LogName);
                    }

                    log.WriteLine(DateTime.Now);
                    log.WriteLine(exc.Message);
                    if (ex.InnerException != null)
                    {
                        log.WriteLine("Inner Exception");
                        log.WriteLine(exc.Message);
                    }
                    log.WriteLine();
                    log.Close();
                }
                catch { }
            }
        }

        private static string GetAssemblyAttribute<T>(Func<T, string> value) where T : Attribute
        {
            T attribute = (T)Attribute.GetCustomAttribute(Assembly.GetExecutingAssembly(), typeof(T));
            return value.Invoke(attribute);
        }
    }
}
