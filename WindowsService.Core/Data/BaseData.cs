﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WindowsService.Core.Utils;
using WindowsService.Core.System;

namespace WindowsService.Core.Data
{
    public class BaseData : Disposable
    {
        protected DaveEntities db;
        public XSystem System { get; private set; }

        public BaseData(XSystem system)
        {
            System = system;
            db = null;
        }

        protected bool Check(string action)
        {
            if (db == null)
            {
                try { db = new DaveEntities(XConfiguration.ConnectionString); }
                catch (Exception ex) { Log(ex, LogErrorID.Configuration, String.Format("Creating EF model exception: {0}", action)); }
            }
            return (db != null);
        }

        protected void Close()
        {
            if (db != null)
            {
                try { db.Dispose(); }
                catch { /* no need to log */ }
                finally { db = null; }
            }
        }

        public bool Save(string action)
        {
            try
            {
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Log(ex, LogErrorID.DataUpdate, string.Format("Error during database update for action '{0}'", action));
                Close();
                return false;
            }
        }

        protected virtual void Log(bool positive, int errorID, string comment)
        {
            EventLogger.Post(positive, errorID, comment);
        }

        protected virtual void Log(Exception ex, int errorID, string comment)
        {
            EventLogger.Post(ex, errorID, comment);
        }

        protected override void DisposeManaged()
        {
            Close();
        }
    }
}
