﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using WindowsService.Engine.Processor;

namespace WindowsService.Engine.Scheduler
{
    public class SystemScheduler
    {
        CustomProcessScheduler customProcessScheduler;
        public volatile Boolean Abort = false;

        /// <summary>
        /// Starts the following processes
        /// 
        /// </summary>
        public void Run()
        {
            ConnectionMonitor.Start();
            //-- Add your own Process Schedulers here
            //-- eg: 
            customProcessScheduler = new CustomProcessScheduler(this);
        }

        /// <summary>
        /// Sets abort variable so inherited threads know to stop
        /// </summary>
        public void Stop()
        {
            Abort = true;
            //-- Abort your customer Process Schedulers here
            //-- eg: 
            while (!customProcessScheduler.Finished) Thread.Sleep(100);
            ConnectionMonitor.Stop();
        }
    }

    abstract class BaseScheduler
    {
        protected SystemScheduler scheduler;
        Thread thread;

        public volatile bool Finished = false;

        public BaseScheduler(SystemScheduler scheduler)
        {
            this.scheduler = scheduler;

            thread = new Thread(Execute);
            thread.Start();
        }

        void Execute()
        {
            try { Run(); }
            finally { Finished = true; }
        }

        protected abstract void Run();
    }
}
