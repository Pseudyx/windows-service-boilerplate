﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using WindowsService.Core.System;
using WindowsService.Engine.Processor;

namespace WindowsService.Engine.Scheduler
{
    class CustomProcessScheduler : BaseScheduler
    {
        public CustomProcessScheduler(SystemScheduler scheduler) : base(scheduler) { }

        protected override void Run()
        {
            using (var system = new XSystem())
            {
                system.ProcessName = "ProcessScheduler";
                try
                {

                    if (ConnectionMonitor.Connected)
                        system.Logger.SystemLog(system.ProcessName, true, LogErrorID.Scheduler, String.Format("{0} started", system.ProcessName));

                    using (var CustomProcessor = new CustomProcessor(system))
                    {
                        while (!scheduler.Abort)
                        {
                            //-- Add Calls to Customer Processor here
                            if (ConnectionMonitor.Connected)
                                CustomProcessor.Execute();


                            //-- Example of pause process timer
                            //After processing, pause process thread for 10min.
                            DateTime wakeTime = DateTime.Now.AddMinutes(10);
                            while (DateTime.Now < wakeTime && !scheduler.Abort)
                                Thread.Sleep(1000);
                        }
                    }

                }
                catch (Exception ex) { system.Logger.SystemLog(system.ProcessName, false, LogErrorID.Unexpected, ex.Message); }
                finally
                {
                    Finished = true; //tell the ControllerService that current cycle has finished, so service can be stopped
                    system.Logger.SystemLog(system.ProcessName, true, LogErrorID.Scheduler, String.Format("{0} stopped", system.ProcessName));
                }
            }
        }
    }
}
