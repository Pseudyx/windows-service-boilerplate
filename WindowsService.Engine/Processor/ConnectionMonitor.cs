﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using WindowsService.Core.Data;
using WindowsService.Core.System;

namespace WindowsService.Engine.Processor
{
    public class ConnectionMonitor
    {
        static volatile bool work = true;
        static Thread thread = null;

        static ConnectionMonitor()
        {
            Connected = false;
            Started = false;
        }

        public static bool Connected { get; private set; }
        public static bool Started { get; private set; }

        static void Execute()
        {
            try
            {
                DateTime next = DateTime.Now;
                using (XSystem system = new XSystem())
                {
                    system.ProcessName = "ConnectionManager";

                    using (Poke poke = new Poke(system))
                    {
                        bool prev;
                        while (work)
                        {
                            if (next < DateTime.Now)
                            {
                                prev = Connected;
                                if (Connected = poke.Do())
                                {
                                    if (!prev || !Started)

                                        system.Logger.SystemLog(system.ProcessName, true, LogErrorID.DatabaseConnectivity, "Database connection establised");

                                }
                                else
                                {
                                    if (prev || !Started)
                                        system.Logger.SystemLog(system.ProcessName, false, LogErrorID.DatabaseConnectivity, "Database connection broken");
                                }
                                Started = true;
                                next = DateTime.Now.AddSeconds(1);
                            }
                            Thread.Sleep(100);
                        }
                    }
                }
            }
            catch (ThreadAbortException) { }
            finally { Started = false; }
        }

        public static void Start()
        {
            if (thread != null) return;

            work = true;
            thread = new Thread(Execute);
            thread.Start();

            while (!Started) Thread.Sleep(100);
        }

        public static void Stop()
        {
            if (thread == null) return;

            work = false;
            try
            {
                thread.Join(2000);
                thread.Abort();
            }
            catch { }
            finally { thread = null; }
        }

    }
}
